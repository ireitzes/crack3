/*

Lab 12: Cracking Passwords Faster
Due Dec 1 by 9am Points 10 Submitting a website url

Requirement: Modifying the previous program to make it go faster by using 
a binary tree to search through its rainbow table.
Required Changes:
You need to change this file:   crack.c

Find the comments labeled TODO. These are things you need to do to complete 
the assignment.

    Read the dictionary into an array of structures containing the original 
        plaintext password and the hash. This structure is called a struct entry
    Sort the dictionary array using qsort. Sort the entries by their hash value,
        so that the hashes are in order. Use the strcmp function to compare 
        hashes.
    Search the array using bsearch and recover the plaintext password.

    Use the Commits tab in BitBucket to see all the different versions we 
    worked on.
Deliverable:
    Commit your changes. Push your changes to BitBucket.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be
const int HASH_LEN=33;

// Stucture to hold both a plaintext password and a hash.
typedef struct entry 
{// TODO: FILL THIS IN
    char password[20]; //[20];  char *password;
    char hash[33]; //[33];      char *hash;
}entry;

int arrayLength = 33;
int compare (const void *a, const void *b)
{
	struct entry *aa = (struct entry *)a;
	struct entry *bb = (struct entry *)b;
	return strcmp(aa->hash, bb->hash);
}

int compare2 (const void *a, const void *b)
{
   return ( *(char*)a - *(char*)b );
}


int compare3(const void * a, const void * b) {
   return ( *(char*)a  - *(char*)b );
}


struct entry *read_dictionary(char *filename, int *size, char *hashname, int *len)
{
    *size = 0;
    arrayLength = 33;
    char line[32];
    int entries = 0;
	char d[20]; 
	
// Set up memory allocation
	entry* arr = malloc(arrayLength * sizeof(entry));

// Verify files
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    perror("Can't open password dictionary file");
	    exit(1);
	}
	
	FILE *hashin = fopen(hashname, "r");
	if (!hashin)
	{
	    perror("Can't open dictionary hashname file");
	    exit(1);
	}

// while loop to find \n and replace with /0
	while(fgets(line, PASS_LEN, in) != NULL)
	{
		char *nl = strchr(line, '\n');      // Find new line in array
		if (nl != NULL)                 // if not NULL
			{
			    *nl = '\0';             	// set null
			}
	entries++;
	
//  reallocate the array memory as needed				
		if (entries == arrayLength)
		{
			arrayLength += 20;
			arr = realloc(arr, (arrayLength+10) * sizeof(*arr));
		}  // End if

// use the md5 to create the password hash and copy that to the struct .hash		
	strcpy(d,line);
	sscanf(d, "%s", arr[entries].password);
	
	char *passhash = md5(d, strlen(d));  // <--md5 algorithm
	
	char y[32];
	strcpy(y,passhash);
	strcpy(arr[entries].hash, y);
	}   // End while

// qsort the array based on hash    
    qsort (arr, entries+1, sizeof(entry), compare);
  
//Print the sorted order of the array based on hash
    printf("Eligible Candidates\n");
	int n = 0;  
	for (n=1; n<entries+1; n++) 
	{   
//		printf ("**Sorted by qsort [%d] Hash = %s    Password = %s \n",n, 
//				arr[n].hash, arr[n].password);
	} 
	*size = entries;
	return arr;
}  // END read_dictionary function


//=====Create an array of the hashes.txt file for comparison for bsearch========
void createSample(entry* dictionary, int length)
{
	#define LSIZ 34
	#define RSIZ 1000
	
	int yy = length;
	printf("length = %d", length);
	char sample[yy][LSIZ];
    FILE *fptr = NULL; 
    int i = 0;
    int tot = 0;

//  Read the file and store the lines into an array

    fptr = fopen("hashes.txt", "r");
    while(fgets(sample[i], LSIZ+2, fptr)) 
	{
        sample[i][strlen(sample[i]) - 1] = '\0';
        i++;
    }
    tot = i;
    printf("tot = %d\n", tot);

    for(i = 0; i < tot; ++i)
    {
        char find[33];
        strcpy(find,sample[i]);
        strcpy(find,dictionary[i].hash);
    }
    
//  Run qsort against the hash array    
    qsort (sample, tot, sizeof(sample[0]), compare2);//  <----New

/* Optional print qsort
    for(i = 0; i < tot; ++i)
    {
        printf("Sorted hashes.txt array [%d] %s\n", i+1, sample[i]);
    }
    printf("\n"); */
    
// bsearch
    char find[34];
    char find2[34];
    char result[34];
    for(int k = 1; k < tot+1; ++k)
    {   
        strcpy(result,sample[k]);
        strcpy(find,dictionary[k].hash);
        strcpy(find2,dictionary[k].password);
        int *found = bsearch(find, sample, length, sizeof(result), compare3);
        
// print bsearch results
	    if (found != NULL)
	    {
			printf ("**After bsearch Entry[%d] Hash = %s    Password =  %s \n",k, find, find2);
		}
		else
    	{
		printf("Entry[%d] %s Not found.\n",k, find);
	    }
    }
    tot = i;
    printf("tot = %d\n", tot);

// close
	fclose(fptr);                          // close the input file

}
// =====================END createSample =============================	


int main(int argc, char *argv[])
{
	int i = 1;
	int dlen;
//	char sample[100];
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
	FILE *h = fopen(argv[i], "r");      // input hashes.txt file
    FILE *f = fopen(argv[i+1], "r");    // input password.txt file
    
   // =========================== Start File Check ============================       
    if (!h)                             // if not file
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);                        // exit
    }
        
    if (!f)                             // if not file
    {
        printf("Can't open %s for reading\n", argv[2]);
        exit(1);                        // exit
    }
    
// fill the structure - Returns array to d
// Performs both md5 password hack and qsort
    struct entry *d = read_dictionary(argv[2], &dlen, argv[1], &dlen);
    
    
// Assign the hashes .txt to an array   qsorts and bsearch.  Returns sample 
// array
    printf("Total Files = %d\n",dlen);
//    char *sample = createSample(d,dlen);
    createSample(d,dlen);
//    printf("sample file 15 is %s\n", &sample[15]);   // Test that array returned correctly

    printf("Total Files = %d\n",dlen);
    	fclose(f);                          // close the input file
        fclose(h);                          // close the input file
    
}            